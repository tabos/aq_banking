#include <gtk/gtk.h>
#include <glib.h>

#include <gwenhywfar/text.h>
#include <gwenhywfar/syncio_file.h>
#include <gwenhywfar/gui_be.h>
#include <gwenhywfar/debug.h>
#include <gwenhywfar/stringlist.h>
#include <gwenhywfar/url.h>
#include <aqbanking/accstatus.h>
#include <aqbanking/account.h>
#include <aqbanking/banking.h>
#include <aqbanking/banking_be.h>
#include <aqbanking/transaction.h>
#include <aqbanking/jobgetbalance.h>
#include <aqbanking/jobgettransactions.h>
#include <aqbanking/value.h>
#include <aqhbci/provider.h>
#include <aqhbci/user.h>

#include "banking-backend.h"
#include "debug.h"

struct _BankingBackend {
  GObject                parent_instance;

  AB_BANKING            *ab;
  AB_IMEXPORTER_CONTEXT *ctx;
  AB_ACCOUNT            *active_account;

  gint                   hbci_version;
  gint                   http_major;
  gint                   http_minor;

  gchar                 *bank_code;
  gchar                 *user_name;
  gchar                 *account_name;
  gchar                 *account_password;
  gchar                 *ctx_file;
};

typedef struct _BankingTransaction {
  gchar  *remote_name;
  gchar  *purpose;
  gchar  *date;
  gdouble value;
  gchar  *transaction_text;
} BankingTransaction;


G_DEFINE_TYPE (BankingBackend, banking_backend, G_TYPE_OBJECT)

struct fints {
  gchar *nr;
  gchar *blz;
  gchar *institute;
  gchar *url;
};

GList *fints_db = NULL;

int
readContext (const char             *ctxFile,
             AB_IMEXPORTER_CONTEXT **pCtx,
             int                     mustExist)
{
  AB_IMEXPORTER_CONTEXT *ctx;
  GWEN_SYNCIO *sio;
  GWEN_DB_NODE *dbCtx;
  int rv;

  if (ctxFile == NULL) {
    sio = GWEN_SyncIo_File_fromStdin ();
    GWEN_SyncIo_AddFlags (sio,
                          GWEN_SYNCIO_FLAGS_DONTCLOSE |
                          GWEN_SYNCIO_FILE_FLAGS_READ);
  } else {
    sio = GWEN_SyncIo_File_new (ctxFile, GWEN_SyncIo_File_CreationMode_OpenExisting);
    GWEN_SyncIo_AddFlags (sio, GWEN_SYNCIO_FILE_FLAGS_READ);
    rv = GWEN_SyncIo_Connect (sio);
    if (rv < 0) {
      if (!mustExist) {
        ctx = AB_ImExporterContext_new ();
        *pCtx = ctx;
        GWEN_SyncIo_free (sio);
        return 0;
      }
      GWEN_SyncIo_free (sio);
      return 4;
    }
  }

  /* actually read */
  dbCtx = GWEN_DB_Group_new ("context");
  rv = GWEN_DB_ReadFromIo (dbCtx, sio,
                           GWEN_DB_FLAGS_DEFAULT |
                           GWEN_PATH_FLAGS_CREATE_GROUP);
  if (rv < 0) {
    g_warning ("Error reading context file (%d)", rv);
    GWEN_DB_Group_free (dbCtx);
    GWEN_SyncIo_Disconnect (sio);
    GWEN_SyncIo_free (sio);
    return rv;
  }
  GWEN_SyncIo_Disconnect (sio);
  GWEN_SyncIo_free (sio);

  ctx = AB_ImExporterContext_fromDb (dbCtx);
  if (!ctx) {
    g_warning ("No context in input data");
    GWEN_DB_Group_free (dbCtx);
    return GWEN_ERROR_BAD_DATA;
  }
  GWEN_DB_Group_free (dbCtx);
  *pCtx = ctx;

  return 0;
}

int
writeContext (const char                  *ctxFile,
              const AB_IMEXPORTER_CONTEXT *ctx)
{
  GWEN_DB_NODE *dbCtx;
  GWEN_SYNCIO *sio;
  int rv;

  if (ctxFile == NULL) {
    sio = GWEN_SyncIo_File_fromStdout ();
    GWEN_SyncIo_AddFlags (sio,
                          GWEN_SYNCIO_FLAGS_DONTCLOSE |
                          GWEN_SYNCIO_FILE_FLAGS_WRITE);
  } else {
    sio = GWEN_SyncIo_File_new (ctxFile, GWEN_SyncIo_File_CreationMode_CreateAlways);
    GWEN_SyncIo_AddFlags (sio,
                          GWEN_SYNCIO_FILE_FLAGS_READ |
                          GWEN_SYNCIO_FILE_FLAGS_WRITE |
                          GWEN_SYNCIO_FILE_FLAGS_UREAD |
                          GWEN_SYNCIO_FILE_FLAGS_UWRITE |
                          GWEN_SYNCIO_FILE_FLAGS_GREAD |
                          GWEN_SYNCIO_FILE_FLAGS_GWRITE);
    rv = GWEN_SyncIo_Connect (sio);
    if (rv < 0) {
      /*DBG_ERROR(0, "Error selecting output file: %s", */
      /*        strerror(errno)); */
      GWEN_SyncIo_free (sio);
      return 4;
    }
  }


  dbCtx = GWEN_DB_Group_new ("context");
  rv = AB_ImExporterContext_toDb (ctx, dbCtx);
  if (rv < 0) {
    DBG_ERROR (0, "Error writing context to db (%d)", rv);
    GWEN_DB_Group_free (dbCtx);
    GWEN_SyncIo_Disconnect (sio);
    GWEN_SyncIo_free (sio);
    return rv;
  }

  rv = GWEN_DB_WriteToIo (dbCtx, sio, GWEN_DB_FLAGS_DEFAULT);
  if (rv < 0) {
    DBG_ERROR (0, "Error writing context (%d)", rv);
  } else {
    rv = 0;
  }

  GWEN_DB_Group_free (dbCtx);
  GWEN_SyncIo_Disconnect (sio);
  GWEN_SyncIo_free (sio);

  return rv;
}

static
AB_ACCOUNT_STATUS *
_getLastAccountStatus (AB_IMEXPORTER_ACCOUNTINFO *iea)
{
  AB_ACCOUNT_STATUS *lastAst = 0;
  const GWEN_TIME *lastTi = 0;
  AB_ACCOUNT_STATUS *ast = 0;

  ast = AB_ImExporterAccountInfo_GetFirstAccountStatus (iea);
  while (ast) {
    const GWEN_TIME *ti;

    if (lastAst && lastTi && (ti = AB_AccountStatus_GetTime (ast))) {
      if (GWEN_Time_Diff (ti, lastTi) > 0) {
        lastAst = ast;
        lastTi = ti;
      }
    } else {
      lastAst = ast;
      lastTi = AB_AccountStatus_GetTime (ast);
    }
    ast = AB_ImExporterAccountInfo_GetNextAccountStatus (iea);
  }

  return lastAst;
}

#define UTF8_COMPUTE(Char, Mask, Len)                                         \
  if (Char < 128)                                                             \
  {                                                                         \
    Len = 1;                                                                \
    Mask = 0x7f;                                                            \
  }                                                                         \
  else if ((Char & 0xe0) == 0xc0)                                             \
  {                                                                         \
    Len = 2;                                                                \
    Mask = 0x1f;                                                            \
  }                                                                         \
  else if ((Char & 0xf0) == 0xe0)                                             \
  {                                                                         \
    Len = 3;                                                                \
    Mask = 0x0f;                                                            \
  }                                                                         \
  else if ((Char & 0xf8) == 0xf0)                                             \
  {                                                                         \
    Len = 4;                                                                \
    Mask = 0x07;                                                            \
  }                                                                         \
  else if ((Char & 0xfc) == 0xf8)                                             \
  {                                                                         \
    Len = 5;                                                                \
    Mask = 0x03;                                                            \
  }                                                                         \
  else if ((Char & 0xfe) == 0xfc)                                             \
  {                                                                         \
    Len = 6;                                                                \
    Mask = 0x01;                                                            \
  }                                                                         \
  else {                                                                        \
    Len = -1;}

#define UTF8_LENGTH(Char)              \
  ((Char) < 0x80 ? 1 :                 \
   ((Char) < 0x800 ? 2 :               \
    ((Char) < 0x10000 ? 3 :            \
     ((Char) < 0x200000 ? 4 :          \
      ((Char) < 0x4000000 ? 5 : 6)))))


#define UTF8_GET(Result, Chars, Count, Mask, Len)                             \
  (Result) = (Chars)[0] & (Mask);                                             \
  for ((Count) = 1; (Count) < (Len); ++(Count))                               \
  {                                                                         \
    if (((Chars)[(Count)] & 0xc0) != 0x80)                                  \
    {                                                                     \
      (Result) = -1;                                                      \
      break;                                                              \
    }                                                                     \
    (Result) <<= 6;                                                         \
    (Result) |= ((Chars)[(Count)] & 0x3f);                                  \
  }
#define UNICODE_VALID(Char)                   \
  ((Char) < 0x110000 &&                             \
   (((Char) & 0xFFFFF800) != 0xD800) &&             \
   ((Char) < 0xFDD0 || (Char) > 0xFDEF) &&          \
   ((Char) >= 0x20 || (Char) == 0x09 || (Char) == 0x0A || (Char) == 0x0D) && \
   ((Char) & 0xFFFE) != 0xFFFE)

gboolean
gnc_utf8_validate (const gchar  *str,
                   gssize        max_len,
                   const gchar **end)
{
  const gchar *p;

  g_return_val_if_fail (str != NULL, FALSE);

  if (end) {
    *end = str;
  }

  p = str;

  while ((max_len < 0 || (p - str) < max_len) && *p) {
    int i, mask = 0, len;
    gunichar result;
    unsigned char c = (unsigned char) *p;

    UTF8_COMPUTE (c, mask, len);

    if (len == -1) {
      break;
    }

    /* check that the expected number of bytes exists in str */
    if (max_len >= 0 &&
        ((max_len - (p - str)) < len)) {
      break;
    }

    UTF8_GET (result, p, i, mask, len);

    if (UTF8_LENGTH (result) != len) {          /* Check for overlong UTF-8 */
      break;
    }

    if (result == (gunichar) - 1) {
      break;
    }

    if (!UNICODE_VALID (result)) {
      break;
    }

    p += len;
  }

  if (end) {
    *end = p;
  }

  /* See that we covered the entire length if a length was
   * passed in, or that we ended on a nul if not
   */
  if (max_len >= 0 &&
      p != (str + max_len)) {
    return FALSE;
  } else if (max_len < 0 &&
             *p != '\0') {
    return FALSE;
  } else {
    return TRUE;
  }
}
void
gnc_utf8_strip_invalid (gchar *str)
{
  gchar *end;
  gint len;

  g_return_if_fail (str);

  if (gnc_utf8_validate (str, -1, (const gchar **) &end)) {
    return;
  }

  g_warning ("Invalid utf8 string: %s", str);
  do {
    len = strlen (end);
    memmove (end, end + 1, len);            /* shuffle the remainder one byte */
  } while (!gnc_utf8_validate (str, -1, (const gchar **) &end));
}

static gpointer
join_ab_strings_cb (const gchar *str,
                    gpointer     user_data)
{
  gchar **acc = user_data;
  gchar *tmp;

  if (!str || !*str) {
    return NULL;
  }

  tmp = g_strdup (str);
  g_strstrip (tmp);
  gnc_utf8_strip_invalid (tmp);

  if (*acc) {
    gchar *join = g_strjoin (" ", *acc, tmp, (gchar *) NULL);
    g_free (*acc);
    g_free (tmp);
    *acc = join;
  } else {
    *acc = tmp;
  }
  return NULL;
}

gchar *
gnc_ab_get_remote_name (const AB_TRANSACTION *ab_trans)
{
  const GWEN_STRINGLIST *ab_remote_name;
  gchar *gnc_other_name = NULL;

  g_return_val_if_fail (ab_trans, NULL);

  ab_remote_name = AB_Transaction_GetRemoteName (ab_trans);
  if (ab_remote_name) {
    GWEN_StringList_ForEach (ab_remote_name, join_ab_strings_cb,
                             &gnc_other_name);
  }

  if (!gnc_other_name || !*gnc_other_name) {
    g_free (gnc_other_name);
    gnc_other_name = NULL;
  }

  return gnc_other_name;
}

   #define GNC_PREFS_GROUP_AQBANKING       "dialogs.import.hbci"
#define GNC_PREF_USE_TRANSACTION_TXT    "use-ns-transaction-text"


gchar *
gnc_ab_get_purpose (const AB_TRANSACTION *ab_trans,
                    gboolean              is_ofx)
{
  const GWEN_STRINGLIST *ab_purpose;
  const char *ab_transactionText = NULL;
  gchar *gnc_description = NULL;

  g_return_val_if_fail (ab_trans, g_strdup (""));

  if (!is_ofx && TRUE /*gnc_prefs_get_bool(GNC_PREFS_GROUP_AQBANKING, GNC_PREF_USE_TRANSACTION_TXT)*/) {
    /* According to AqBanking, some of the non-swift lines have a special
     * meaning. Some banks place valuable text into the transaction text,
     * hence we put this text in front of the purpose. */
    ab_transactionText = AB_Transaction_GetTransactionText (ab_trans);
    if (ab_transactionText) {
      gnc_description = g_strdup (ab_transactionText);
    }
  }

  ab_purpose = AB_Transaction_GetPurpose (ab_trans);
  if (ab_purpose) {
    GWEN_StringList_ForEach (ab_purpose, join_ab_strings_cb,
                             &gnc_description);
  }

  if (!gnc_description) {
    gnc_description = g_strdup ("");
  }

  return gnc_description;
}

double
gnc_ab_get_value (const AB_TRANSACTION *ab_trans)
{
  g_return_val_if_fail (ab_trans, 0);

  return AB_Value_GetValueAsDouble (AB_Transaction_GetValue (ab_trans));
}


gchar *
gnc_ab_get_date (const AB_TRANSACTION *ab_trans)
{
  const GWEN_TIME *time = AB_Transaction_GetDate (ab_trans);

  struct tm tm = GWEN_Time_toTm (time);

  return g_strdup_printf ("%2.2d.%2.2d.%4.4d", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);
}

gchar *
gnc_ab_get_transaction_text (const AB_TRANSACTION *ab_trans)
{
  const gchar *text = AB_Transaction_GetTransactionText (ab_trans);

  return g_strdup (text != NULL ? text : "");
}

static AB_IMEXPORTER_ACCOUNTINFO *
get_iea (BankingBackend *self,
         AB_ACCOUNT     *account)
{
  AB_IMEXPORTER_ACCOUNTINFO *iea;
  const gchar *bankId = AB_Account_GetBankCode (account);
  const gchar *bankName = AB_Account_GetBankName (account);
  const gchar *accountId = AB_Account_GetAccountNumber (account);
  gchar *accountName = NULL;

  if (self->ctx == NULL) {
    return NULL;
  }

  iea = AB_ImExporterContext_GetFirstAccountInfo (self->ctx);

  while (iea) {
    int matches = 1;
    const char *s;

    if (matches && bankId) {
      s = AB_ImExporterAccountInfo_GetBankCode (iea);
      if (!s || !*s || -1 == GWEN_Text_ComparePattern (s, bankId, 0)) {
        matches = 0;
      }

      if (s && *s) {
        TRACE_MSG ("BankID %s", s);
      }
    }

    if (matches && bankName) {
      s = AB_ImExporterAccountInfo_GetBankName (iea);
      if (!s || !*s) {
        s = AB_ImExporterAccountInfo_GetBankName (iea);
      }
      if (!s || !*s || -1 == GWEN_Text_ComparePattern (s, bankName, 0)) {
        matches = 0;
      }
      if (s && *s) {
        TRACE_MSG ("BankName %s", s);
      }
    }

    if (matches && accountId) {
      s = AB_ImExporterAccountInfo_GetAccountNumber (iea);
      if (!s || !*s || -1 == GWEN_Text_ComparePattern (s, accountId, 0)) {
        matches = 0;
      }
      if (s && *s) {
        TRACE_MSG ("AccountNumber %s %d", s, matches);
      }
    }
    if (matches && accountName) {
      TRACE_MSG ("Get account name 1");
      s = AB_ImExporterAccountInfo_GetAccountName (iea);
      TRACE_MSG ("Get account name 2");
      if (!s || !*s) {
        s = AB_ImExporterAccountInfo_GetAccountName (iea);
      }
      TRACE_MSG ("Get account name 3");
      if (s && *s) {
        TRACE_MSG ("AccountName %s", s);
      }
      if (!s || !*s || -1 == GWEN_Text_ComparePattern (s, accountName, 0)) {
        matches = 0;
      }
    }
    if (matches) {
      return iea;
    }
    iea = AB_ImExporterContext_GetNextAccountInfo (self->ctx);
  }

  return NULL;
}
int
getBankUrl (AB_BANKING    *ab,
            AH_CRYPT_MODE  cm,
            const char    *bankId,
            GWEN_BUFFER   *bufServer)
{
  AB_BANKINFO *bi;
  if (!strcmp (bankId, "25050000")) {
    TODO (" ************** 25050000 hack\n");
    GWEN_Buffer_Reset (bufServer);
    GWEN_Buffer_AppendString (bufServer, "https://banking-li1.s-fints-pt-li.de/fints30");
    return 0;
  }
  bi = AB_Banking_GetBankInfo (ab, "de", 0, bankId);
  if (bi) {
    AB_BANKINFO_SERVICE_LIST *l;
    AB_BANKINFO_SERVICE *sv;

    l = AB_BankInfo_GetServices (bi);
    assert (l);
    sv = AB_BankInfoService_List_First (l);
    while (sv) {
      const char *st;

      st = AB_BankInfoService_GetType (sv);
      if (st && *st && strcasecmp (st, "hbci") == 0) {
        const char *svm;

        svm = AB_BankInfoService_GetMode (sv);
        if (svm && *svm) {
          if (!
              ((strcasecmp (svm, "pintan") == 0) ^
               (cm == AH_CryptMode_Pintan))) {
            const char *addr;

            addr = AB_BankInfoService_GetAddress (sv);
            if (addr && *addr) {
              GWEN_Buffer_Reset (bufServer);
              GWEN_Buffer_AppendString (bufServer, addr);
              return 0;
            }
          }
        }
      }
      sv = AB_BankInfoService_List_Next (sv);
    }
    AB_BankInfo_free (bi);
  }

  return -1;
}

GList *
banking_account_get_transactions (BankingBackend *self,
                                  BankingAccount *account)
{
  GList *list = NULL;
  AB_IMEXPORTER_ACCOUNTINFO *iea = get_iea (self, account);
  AB_TRANSACTION *transaction;

  if (iea == NULL) {
    return NULL;
  }

  transaction = AB_ImExporterAccountInfo_GetFirstTransaction (iea);

  while (transaction != NULL) {
    BankingTransaction *trans = g_slice_new0 (BankingTransaction);

    trans->remote_name = gnc_ab_get_remote_name (transaction);
    trans->purpose = gnc_ab_get_purpose (transaction, TRUE);
    trans->value = gnc_ab_get_value (transaction);
    trans->date = gnc_ab_get_date (transaction);
    trans->transaction_text = gnc_ab_get_transaction_text (transaction);

    list = g_list_append (list, trans);
    transaction = AB_ImExporterAccountInfo_GetNextTransaction (iea);
  }

  return list;
}

const gchar *
banking_transaction_get_remote_name (BankingTransaction *self)
{
  return self->remote_name;
}

const gchar *
banking_transaction_get_purpose (BankingTransaction *self)
{
  return self->purpose;
}

const gchar *
banking_transaction_get_date (BankingTransaction *self)
{
  return self->date;
}

gdouble
banking_transaction_get_value (BankingTransaction *self)
{
  return self->value;
}

const gchar *
banking_transaction_get_transaction_text (BankingTransaction *self)
{
  return self->transaction_text;
}

gdouble
get_booked_balance (BankingBackend *self,
                    AB_ACCOUNT     *account)
{
  AB_IMEXPORTER_ACCOUNTINFO *iea = NULL;
  AB_ACCOUNT_STATUS *ast;
  gdouble ret_val = 0;

  iea = get_iea (self, account);
  TRACE_MSG ("Got IEA %p", iea);
  if (iea != NULL) {
    ast = _getLastAccountStatus (iea);
    if (ast == NULL) {
      TODO ("REALLY\n");
    }

    const AB_BALANCE *bal = AB_AccountStatus_GetBookedBalance (ast);
    TRACE_MSG ("Got balance %p", bal);
    if (bal == NULL) {
      bal = AB_AccountStatus_GetNotedBalance (ast);
    }
    if (bal != NULL) {
      const AB_VALUE *val = AB_Balance_GetValue (bal);
      ret_val = AB_Value_GetValueAsDouble (val);
    } else {
      TODO ("AGAIN");
    }
  }

  RETURN (ret_val);
  return ret_val;
}

void
load_fints_db (BankingBackend *self)
{
  GBytes *gbytes = g_resources_lookup_data ("/org/tabos/banking/fints_institute.csv", G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  GtkListStore *ls = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  GtkTreeIter iter;
  gconstpointer data = g_bytes_get_data (gbytes, NULL);
  gchar **lines = NULL;
  gint index = 0;
  gchar sep[2];

  sep[0] = ';';
  sep[1] = '\0';
  lines = g_strsplit (data, "\n", -1);
  while (lines[++index] != NULL) {
    gchar **split = g_strsplit (lines[index], sep, -1);
    struct fints *fints;

    if (g_strv_length (split) != 31) {
      continue;
    }
    if (strlen (split[6]) <= 0) {
      continue;
    }

    fints = g_slice_new0 (struct fints);
    fints->nr = g_strdup (split[0]);
    fints->blz = g_strdup (split[1]);
    fints->institute = g_strdup (split[2]);
    fints->url = g_strdup (split[23]);  /*6 vs 23 */
    /*g_print ("Adding %s/%s/%s/%s\n", fints->nr, fints->blz, fints->institute, fints->url); */

    gtk_list_store_append (ls, &iter);
    gtk_list_store_set (ls, &iter, 0, fints->blz, 1, fints, -1);

    fints_db = g_list_append (fints_db, fints);
  }

  /*gtk_entry_completion_set_model(self->bank_completion, GTK_TREE_MODEL(ls)); */
}

typedef struct GWEN_GUI_BANKING GWEN_GUI_BANKING;
struct GWEN_GUI_BANKING {
  BankingBackend *self;
};


GWEN_INHERIT (GWEN_GUI, GWEN_GUI_BANKING)

GWENHYWFAR_CB void
gui_banking_free_data (void *bp,
                       void *p)
{
  GWEN_GUI_BANKING *xgui;

  xgui = (GWEN_GUI_BANKING *) p;

  GWEN_FREE_OBJECT (xgui);
}


GWEN_GUI *
gwen_gui_banking_new (BankingBackend *self)
{
  GWEN_GUI *gui;
  GWEN_GUI_BANKING *banking_gui;

  TRACE_MSG ("Creating GWEN GUI");
  gui = GWEN_Gui_new ();
  GWEN_NEW_OBJECT (GWEN_GUI_BANKING, banking_gui);
  GWEN_INHERIT_SETDATA (GWEN_GUI, GWEN_GUI_BANKING, gui, banking_gui, gui_banking_free_data);
  banking_gui->self = self;

  return gui;
}

int GWENHYWFAR_CB
check_cert (GWEN_GUI                *gui,
            const GWEN_SSLCERTDESCR *cert,
            GWEN_SYNCIO             *sio,
            uint32_t                 guiid)
{
  TRACE_MSG ("Accepting any cert");
  /* automatically accept any cert for non-interactive checks */
  return 0;
}

gchar *
extract_html (const gchar *text)
{
  gchar *start;
  gchar *end;

  start = strstr (text, "<html>");

  if (start != NULL) {
    end = strstr (start, "</html>");
    if (end != NULL) {
      gint len = end - start - 10;
      gchar *ret = g_malloc0 (len);

      memcpy (ret, start + 6, len);
      return ret;
    }
  }

  return g_strdup (text);
}

GtkWidget *hdy_dialog_new (GtkWindow *parent);
extern GtkApplication *app;

int
get_password (GWEN_GUI                 *gui,
              uint32_t                  flags,
              const char               *token,
              const char               *title,
              const char               *text,
              char                     *buffer,
              int                       minLen,
              int                       maxLen,
              uint32_t                  guiid)
{
  GWEN_GUI_BANKING *banking;
  gint ret = 1;

  banking = GWEN_INHERIT_GETDATA (GWEN_GUI, GWEN_GUI_BANKING, gui);

  assert (maxLen > 5);

  if (banking->self->account_password != NULL) {
    strcpy (buffer, banking->self->account_password);
    return 0;
  } else {
    TRACE_MSG ("app %p", app);
    GtkWindow *window = gtk_application_get_active_window (app);
    TRACE_MSG ("window %p", window);
    GtkWidget *dialog = hdy_dialog_new (window);
    gtk_dialog_add_buttons (GTK_DIALOG (dialog), "OK", GTK_RESPONSE_OK, "Cancel", GTK_RESPONSE_CANCEL, NULL);

    GtkWidget *content = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
    GtkWidget *label;
    GtkWidget *password;
    gint response;
    gchar *markup = extract_html (text);

    gtk_widget_set_margin_top (content, 12);
    gtk_widget_set_margin_bottom (content, 12);
    gtk_widget_set_margin_start (content, 12);
    gtk_widget_set_margin_end (content, 12);
    label = gtk_label_new ("");
    gtk_label_set_markup (GTK_LABEL (label), markup);
    gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
    gtk_box_pack_start (GTK_BOX (content), label, TRUE, TRUE, 6);

    password = gtk_entry_new ();
    gtk_entry_set_visibility (GTK_ENTRY (password), FALSE);
    gtk_entry_set_max_width_chars (GTK_ENTRY (password), 10);
    gtk_box_pack_start (GTK_BOX (content), password, FALSE, FALSE, 6);
    gtk_widget_show_all (content);

    response = gtk_dialog_run (GTK_DIALOG (dialog));
    if (response == GTK_RESPONSE_OK) {
      banking->self->account_password = g_strdup (gtk_entry_get_text (GTK_ENTRY (password)));
      strcpy (buffer, banking->self->account_password);
      ret = 0;
    }
    gtk_widget_destroy (dialog);
  }

  return ret;
}

void
banking_backend_init (BankingBackend *self)
{
  GWEN_GUI *gui;
  int rv;
  const gchar *ctx_file;
  GWEN_DB_NODE *db;
  GWEN_DB_NODE *dbargs = GWEN_DB_Group_new ("arguments");

  gui = gwen_gui_banking_new (self);
  GWEN_Gui_SetCheckCertFn (gui, check_cert);
  GWEN_Gui_SetGetPasswordFn (gui, get_password);
  GWEN_Gui_SetGui (gui);

  self->ab = AB_Banking_new ("GNOME Banking", 0, 0);
  self->hbci_version = 300;
  self->http_major = 1;
  self->http_minor = 2;

  if (AB_Banking_Init (self->ab)) {
    g_error ("Error on init aqbanking");
  }

  if (AB_Banking_OnlineInit (self->ab)) {
    g_error ("Error on onlineinit of aqbanking");
  }

  db = GWEN_DB_GetGroup (dbargs, GWEN_DB_FLAGS_DEFAULT, "local");
  if (db == NULL) {
    TODO ("DB is null\n");
  }

  ctx_file = GWEN_DB_GetCharValue (db, "ctxfile", 0, 0);
  if (ctx_file == NULL) {
    self->ctx_file = g_strdup_printf ("%s/gnome-banking.ctx", g_get_user_cache_dir ());
    GWEN_DB_SetCharValue (db, GWEN_DB_FLAGS_DEFAULT, "ctxfile", self->ctx_file);
    ctx_file = GWEN_DB_GetCharValue (db, "ctxfile", 0, 0);
  } else {
    self->ctx_file = g_strdup (ctx_file);
  }

  rv = readContext (self->ctx_file, &self->ctx, 1);
  if (rv < 0) {
    g_warning ("Error reading context (%d)", rv);
  }

  /*load_fints_db(self); */
}

BankingUser *
banking_backend_create_user (BankingBackend *self,
                             const gchar    *bank_code,
                             const gchar    *account)
{
  GWEN_BUFFER *nameBuffer = NULL;
  AB_PROVIDER *pro;
  AH_CRYPT_MODE cm;
  GWEN_URL *url;
  GWEN_CRYPT_TOKEN_CONTEXT *ctx = NULL;
  AB_USER *user;
  const char *tokenName;
  const char *tokenType;
  const char *bankId;
  const char *userId;
  const char *customerId;
  const char *server;
  const char *userName;
  const char *lbankId;
  const char *luserId;
  const char *lcustomerId;
  const char *lserverAddr;
  uint32_t cid;
  int hbciVersion;
  int rdhType;

  pro = AB_Banking_GetProvider (self->ab, "aqhbci");
  g_return_val_if_fail (pro != NULL, NULL);

  self->account_password = NULL;

#warning Check parameters
  tokenType = "pintan";/*GWEN_DB_GetCharValue(db, "tokenType", 0, 0); */
  tokenName = 0;/*GWEN_DB_GetCharValue(db, "tokenName", 0, 0); */
  bankId = bank_code;/*GWEN_DB_GetCharValue(db, "bankId", 0, 0); */
  userId = account;/*GWEN_DB_GetCharValue(db, "userId", 0, 0); */
  customerId = 0;/* GWEN_DB_GetCharValue(db, "customerId", 0, 0); */
  server = 0;/*GWEN_DB_GetCharValue(db, "serverAddr", 0, 0); */
  cid = 1;/*GWEN_DB_GetIntValue(db, "context", 0, 1); */
  hbciVersion = 300;/*GWEN_DB_GetIntValue(db, "hbciVersion", 0, 0); */
  rdhType = 1;/*GWEN_DB_GetIntValue(db, "rdhType", 0, 1); */
  userName = "GNOME Banking";/*GWEN_DB_GetCharValue(db, "userName", 0, 0); */
  assert (userName);

  /* generic check for some arguments */
  if (hbciVersion > 0 && rdhType > 1) {
    if (hbciVersion < 300 && rdhType > 1) {
      g_warning ("%s(): RDH Types 2 and above only work with HBCI version 300 or later", __FUNCTION__);
      return NULL;
    }
  }

  if (hbciVersion > 0) {
    switch (hbciVersion) {
      case 201:
      case 210:
      case 220:
      case 300:
        /* supported */
        break;

      default:
        g_warning ("%s(): HBCI/FinTS version %d not supported", __FUNCTION__, hbciVersion);
        return NULL;
    }
  }

  if (rdhType > 0) {
    switch (rdhType) {
      case 1:
      case 2:
      case 9:
      case 10:
        /* supported */
        break;

      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      default:
        g_warning ("%s(): RDH type %d not supported", __FUNCTION__, rdhType);
        return NULL;
    }
  }

  if (strcasecmp (tokenType, "pintan") == 0) {
    lbankId = bankId;
    luserId = userId;
    lcustomerId = customerId ? customerId : luserId;
    lserverAddr = server;
    cm = AH_CryptMode_Pintan;
  } else {
    g_error ("%s(): NOT IMPLEMENTED\n", __FUNCTION__);
  }

  if (lbankId == NULL || !*lbankId) {
    g_warning ("%s(): No bank id stored and none given", __FUNCTION__);
    GWEN_Crypt_Token_Context_free (ctx);
    GWEN_Buffer_free (nameBuffer);

    return NULL;
  }

  if (!luserId || !*luserId) {
    g_warning ("%s(): No user id (Benutzerkennung) stored and none given", __FUNCTION__);
    GWEN_Crypt_Token_Context_free (ctx);
    GWEN_Buffer_free (nameBuffer);

    return NULL;
  }

  user = AB_Banking_FindUser (self->ab, AH_PROVIDER_NAME, "de", bank_code, account, account);
  if (user != NULL) {
    g_warning ("%s(): User %s already exists", __FUNCTION__, account);
    GWEN_Crypt_Token_Context_free (ctx);

    return NULL;
  }

  TRACE_MSG (" Creating new user");

  user = AB_Banking_CreateUser (self->ab, AH_PROVIDER_NAME);

  AB_User_SetUserName (user, userName);
  AB_User_SetCountry (user, "de");
  AB_User_SetBankCode (user, lbankId);
  AB_User_SetUserId (user, luserId);
  AB_User_SetCustomerId (user, lcustomerId);
  AH_User_SetTokenType (user, tokenType);
  AH_User_SetTokenName (user, tokenName);
  AH_User_SetTokenContextId (user, cid);
  AH_User_SetCryptMode (user, cm);

  if (rdhType > 0) {
    AH_User_SetRdhType (user, rdhType);
  }

  GWEN_Buffer_free (nameBuffer);

  if (hbciVersion == 0) {
    if (cm == AH_CryptMode_Pintan) {
      AH_User_SetHbciVersion (user, 220);
    } else {
      if (rdhType > 1) {
        AH_User_SetHbciVersion (user, 300);
      } else {
        AH_User_SetHbciVersion (user, 210);
      }
    }
  } else {
    AH_User_SetHbciVersion (user, hbciVersion);
  }

  /* try to get server address from database if still unknown */
  if (lserverAddr == NULL || *lserverAddr == 0) {
    GWEN_BUFFER *tbuf;

    tbuf = GWEN_Buffer_new (0, 256, 0, 1);
    if (getBankUrl (self->ab, cm, lbankId, tbuf)) {
      g_warning ("%s(): Could not find server address for \"%s\"", __FUNCTION__, lbankId);
    }
    if (GWEN_Buffer_GetUsedBytes (tbuf) == 0) {
      g_warning ("%s(): No address given and none available in internal db", __FUNCTION__);
      GWEN_Crypt_Token_Context_free (ctx);

      return NULL;
    }

    url = GWEN_Url_fromString (GWEN_Buffer_GetStart (tbuf));
    if (url == NULL) {
      g_warning ("%s(): Bad URL \"%s\" in internal db", __FUNCTION__, GWEN_Buffer_GetStart (tbuf));
      GWEN_Crypt_Token_Context_free (ctx);

      return NULL;
    }

    GWEN_Buffer_free (tbuf);
  } else {
    /* set address */
    url = GWEN_Url_fromString (lserverAddr);
    if (url == NULL) {
      g_warning ("%s(): Bad URL \"%s\"", __FUNCTION__, lserverAddr);
      GWEN_Crypt_Token_Context_free (ctx);

      return NULL;
    }
  }

  if (cm == AH_CryptMode_Pintan) {
    GWEN_Url_SetProtocol (url, "https");
    if (GWEN_Url_GetPort (url) == 0) {
      GWEN_Url_SetPort (url, 443);
    }
  } else {
    GWEN_Url_SetProtocol (url, "hbci");
    if (GWEN_Url_GetPort (url) == 0) {
      GWEN_Url_SetPort (url, 3000);
    }
  }

  AH_User_SetServerUrl (user, url);
  GWEN_Url_free (url);

  if (cm == AH_CryptMode_Ddv) {
    AH_User_SetStatus (user, AH_UserStatusEnabled);
  }

  AB_Banking_AddUser (self->ab, user);

  /* context no longer needed */
  GWEN_Crypt_Token_Context_free (ctx);

  return user;
}

void
banking_user_get_sysid (BankingBackend *self,
                        BankingUser    *user)
{
  AB_PROVIDER *pro;
  AB_USER *u = user;
  AB_IMEXPORTER_CONTEXT *ctx;
  gint rv;

  ENTRY;

  pro = AB_Banking_GetProvider (self->ab, "aqhbci");
  assert (pro);

  rv = AH_Provider_GetCert (pro, u, 1, 0, 1);
  if (rv < 0) {
    g_warning ("%s(): Error getting certificate (%d)", __FUNCTION__, rv);
    return;
  }

  ctx = AB_ImExporterContext_new ();
  rv = AH_Provider_GetSysId (pro, u, ctx, 1, 0, 1);
  AB_ImExporterContext_free (ctx);
  if (rv) {
    g_warning ("%s(): Error getting system id (%d)", __FUNCTION__, rv);
    return;
  }

  EXIT;
}

void
banking_user_get_accounts (BankingBackend *self,
                           BankingUser    *user)
{
  AB_IMEXPORTER_CONTEXT *ctx;
  AB_PROVIDER *pro;
  gint rv;

  ENTRY;
  pro = AB_Banking_GetProvider (self->ab, "aqhbci");
  assert (pro);

  ctx = AB_ImExporterContext_new ();
  rv = AH_Provider_GetAccounts (pro, user, ctx, 1, 0, 1);
  AB_ImExporterContext_free (ctx);

  if (rv != 0) {
    g_warning ("%s(): Error getting accounts (%d)", __FUNCTION__, rv);
    EXIT;
    return;
  }

  EXIT;
}

static void
banking_backend_finalize (GObject *obj)
{
  BankingBackend *self = BANKING_BACKEND (obj);
  gint rv;

  ENTRY;
  rv = AB_Banking_OnlineFini (self->ab);
  if (rv) {
    g_warning ("%s(): Error on deinit (%d)\n", __FUNCTION__, rv);
    EXIT;
    return;
  }

  rv = AB_Banking_Fini (self->ab);
  if (rv) {
    g_warning ("%s(): Error on deinit (%d)\n", __FUNCTION__, rv);
    EXIT;
    return;
  }

  EXIT;
}

static void
banking_backend_class_init (BankingBackendClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = banking_backend_finalize;
}

BankingBackend *
banking_backend_new (void)
{
  return g_object_new (BANKING_TYPE_BACKEND, NULL);
}

gint
banking_backend_get_num_accounts (BankingBackend *self)
{
  AB_ACCOUNT_LIST2 *accounts = AB_Banking_GetAccounts (self->ab);
  gint num = 0;

  ENTRY;
  if (accounts != NULL) {
    num = AB_Account_List_GetCount (accounts);

    AB_Account_List2_free (accounts);
  }

  RETURN (num);
}

GList *
banking_backend_get_accounts (BankingBackend *self)
{
  GList *list = NULL;
  AB_ACCOUNT_LIST2 *accounts;
  AB_ACCOUNT_LIST2_ITERATOR *it;
  AB_ACCOUNT *a;

  ENTRY;
  accounts = AB_Banking_GetAccounts (self->ab);

  if (accounts == NULL) {
    RETURN(NULL);
  }

  it = AB_Account_List2_First (accounts);
  if (it == NULL) {
    RETURN(NULL);
  }

  a = AB_Account_List2Iterator_Data (it);

  while (a != NULL) {
    list = g_list_append (list, a);
    a = AB_Account_List2Iterator_Next (it);
  }

 RETURN(list);
}

const gchar *
banking_account_get_name (BankingAccount *self)
{
  return AB_Account_GetAccountName (self);
}

const gchar *
banking_account_get_number (BankingAccount *self)
{
  return AB_Account_GetAccountNumber (self);
}

gdouble
banking_banking_get_account_balance (BankingBackend *self,
                                     BankingAccount *account)
{
  return get_booked_balance (self, account);
}

void
banking_user_load_balance (BankingBackend *self,
                           BankingUser    *user)
{
  AB_ACCOUNT_LIST2 *al;
  AB_ACCOUNT_LIST2_ITERATOR *ait;
  AB_ACCOUNT *account;
  AB_JOB *job;
  AB_JOB_LIST2 *job_list;
  AB_IMEXPORTER_CONTEXT *ctx;
  int rv;

  job_list = AB_Job_List2_new ();

  ENTRY;
  al = AB_Banking_GetAccounts (self->ab);
  if (al == NULL) {
    EXIT;
    return;
  }

  ait = AB_Account_List2_First (al);
  if (ait == NULL) {
    EXIT;
    return;
  }

  account = AB_Account_List2Iterator_Data (ait);
  while (account != NULL) {
    job = AB_JobGetBalance_new (account);

    rv = AB_Job_CheckAvailability (job);
    if (rv >= 0) {
      AB_Job_List2_PushBack (job_list, job);
    } else {
      g_warning ("%s(): Error requesting balance %d", __FUNCTION__, rv);
      AB_Job_List2_FreeAll (job_list);
      EXIT;
      return;
    }

    job = AB_JobGetTransactions_new (account);
    rv = AB_Job_CheckAvailability (job);
    if (rv >= 0) {
      AB_Job_List2_PushBack (job_list, job);
    } else {
      g_warning ("%s(): Error requesting balance %d", __FUNCTION__, rv);
      AB_Job_List2_FreeAll (job_list);
      EXIT;
      return;
    }

    account = AB_Account_List2Iterator_Next (ait);
  }

  AB_Account_List2Iterator_free (ait);
  AB_Account_List2_free (al);

  ctx = AB_ImExporterContext_new ();
  rv = AB_Banking_ExecuteJobs (self->ab, job_list, ctx);
  AB_Job_List2_FreeAll (job_list);
  if (rv) {
    g_warning ("%s(): Error on executeQueue (%d)\n", __FUNCTION__, rv);
    AB_ImExporterContext_free (ctx);
  }

  rv = writeContext (self->ctx_file, ctx);
  AB_ImExporterContext_free (ctx);
  if (rv < 0) {
    g_warning ("%s(): Error writing context file (%d)", __FUNCTION__, rv);
  }

  EXIT;
}
