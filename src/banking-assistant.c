/* banking-assistant.c
 *
 * Copyright 2019 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>

#define HANDY_USE_UNSTABLE_API
#include <libhandy-0.0/handy.h>

#include "banking-assistant.h"
#include "banking-backend.h"
#include "banking-window.h"

struct BankingAssistant {
  GtkWidget *dialog;
  GtkWidget *bank_entry;
  GtkWidget *account_entry;
  BankingWindow *window;
  BankingBackend *backend;
  BankingUser *user;
};

typedef struct BankingAssistant BankingAssistant;

#define BANKING_ASSISTANT(x) ((BankingAssistant *)(x))

static void
dialog_close_cb (GtkDialog *dialog,
                 gint       response_id,
                 gpointer   user_data)
{
  gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
login_button_clicked (GtkWidget *widget,
                      gpointer   user_data)
{
  BankingAssistant *self = BANKING_ASSISTANT (user_data);

  self->user = banking_backend_create_user (self->backend,
                                      gtk_entry_get_text (GTK_ENTRY (self->bank_entry)),
                                      gtk_entry_get_text (GTK_ENTRY (self->account_entry)));

  gtk_widget_destroy (self->dialog);

  banking_user_get_sysid (self->backend, self->user);
  banking_user_get_accounts (self->backend, self->user);
  banking_user_load_balance (self->backend, self->user);

  banking_window_load_accounts (self->window);
}

void
banking_assistant_new (BankingWindow  *window,
                       BankingBackend *backend)
{
  GtkWidget *assistant_dialog;
  GtkWidget *grid;
  GtkWidget *bank_label;
  GtkWidget *account_label;
  BankingAssistant *self = g_malloc0 (sizeof (BankingAssistant));

  g_print ("%s(): Called with %p/%p\n", __FUNCTION__, window, backend);
  self->window = window;
  self->backend = backend;

  assistant_dialog = hdy_dialog_new (GTK_WINDOW (window));
  gtk_window_set_title (GTK_WINDOW (assistant_dialog), "Assistant");
  //gtk_dialog_add_buttons (GTK_DIALOG (assistant_dialog),
    //                    "Login", GTK_RESPONSE_ACCEPT,
      //                  "Cancel", GTK_RESPONSE_CANCEL,
        //                NULL);
  g_signal_connect (G_OBJECT (assistant_dialog), "response", G_CALLBACK (dialog_close_cb), self);
  self->dialog = assistant_dialog;

  grid = gtk_grid_new ();
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_widget_set_valign (grid, GTK_ALIGN_CENTER);
  gtk_container_set_border_width (GTK_CONTAINER (grid), 12);

  bank_label = gtk_label_new ("Bank:");
  gtk_widget_set_sensitive (bank_label, FALSE);
  gtk_widget_show (bank_label);
  gtk_grid_attach (GTK_GRID (grid), bank_label, 0, 0, 1, 1);

  self->bank_entry = gtk_entry_new ();
  gtk_widget_set_hexpand (self->bank_entry, TRUE);
  gtk_widget_show (self->bank_entry);
  gtk_grid_attach (GTK_GRID (grid), self->bank_entry, 1, 0, 1, 1);

  account_label = gtk_label_new ("Account:");
  gtk_widget_set_sensitive (account_label, FALSE);
  gtk_widget_show (account_label);
  gtk_grid_attach (GTK_GRID (grid), account_label, 0, 1, 1, 1);

  self->account_entry = gtk_entry_new ();
  gtk_widget_show (self->account_entry);
  gtk_grid_attach (GTK_GRID (grid), self->account_entry, 1, 1, 1, 1);

  GtkWidget *button = gtk_button_new_with_label ("Login");
  gtk_style_context_add_class(gtk_widget_get_style_context(button), GTK_STYLE_CLASS_SUGGESTED_ACTION);
  g_signal_connect (button, "clicked", G_CALLBACK (login_button_clicked), self);
  gtk_widget_show (button);
  gtk_grid_attach (GTK_GRID (grid), button, 1, 2, 1, 1);

  gtk_container_add (GTK_CONTAINER (gtk_dialog_get_content_area (GTK_DIALOG (assistant_dialog))), grid);

  gtk_widget_show (grid);
  gtk_widget_show (assistant_dialog);
}
